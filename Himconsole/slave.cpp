// Copyright 2019 SMS
// License(GPL)
// Author: ShenMian
// ���ض�

#include "slave.h"

uint	 Slave::lastId_	= 0;
ushort Slave::size_		= 0;


Slave::Slave(SOCKET sock)
{
	size_++;
	lastId_++;
	sock_ = sock;
}

Slave::~Slave()
{
	size_--;
}


ushort Slave::size()
{
	return size_;
}


uint Slave::lastId()
{
	return lastId_;
}


ushort Slave::id()
{
	return id_;
}

void Slave::os_info(string& os_info)
{
	memcpy(&os_info_, os_info.c_str(), os_info.size());
}
