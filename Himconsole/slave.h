// Copyright 2019 SMS
// License(GPL)
// Author: ShenMian
// 被控端

#include "tcp_client.h"
#include "type.h"

#ifndef SLAVE_H_
#define SLAVE_H_


class Slave : public TCPClient
{
public:
	Slave(SOCKET);
	~Slave();

	ushort id();
	uint	 lastId();

	void	 os_info(string&);
	ushort os_lang();
	ushort os_mem_total();
	ushort os_mem_free();
	ushort os_mem_available();

	static ushort size();

private:
	ushort		id_;
	char			os_info_[os_info_size];
	user_info user_;

	static uint		lastId_;// 最新会话ID
	static ushort size_;	// 实例总数
};


#endif	// SLAVE_H_
